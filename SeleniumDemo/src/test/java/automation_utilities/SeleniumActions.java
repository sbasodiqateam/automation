package automation_utilities;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumActions extends GenericActions {

	public static WebDriver driver = null;
	public String url = "";

	public static void invokeBrowser() {
		try {
			log.info(" Invoking chrome browser ");
			System.setProperty("webdriver.chrome.driver", "./src/test/resources/Automation_Drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while invoking browser");
		}
	}

	public static void openUrl(String appUrl) {
		try {
			log.info(" opening the url ");
			driver.get(appUrl);
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while opening url");
		}
	}

	public static List<WebElement> getElements(By by){
		try {
			log.info(" fetching list of web elements ");
			return driver.findElements(by);
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while opening url");
			return null;
		}
	}
	
	public static void clickElement(By by) {
		try {
			log.info(" clicking on the element ");
			getElement(by).click();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while Clicking element");
		}
	}

	public static void enterText(By by, String text) {
		
		try {
			log.info(" entering text through keyboard ");
			getElement(by).sendKeys(text);
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while entering text through keyboard");
		}
	}

	public static void submit(By by) {
		try {
			log.info(" submitting the page ");
			getElement(by).submit();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while submitting the page");
		}
	}

	public static String getPageTitle() {
		
		try {
			log.info(" fetching page title ");
			return driver.getTitle();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while fetching the page title");
			return null;
		}
	}

	public static void closeBrowser() {
		try {
			log.info("closing the browser instance");
			driver.close();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while closing the browser instance");
		}
	}
	
	public static void exitBrowser() {
		try {
			log.info("closing all browser instances");
			driver.quit();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while closing all browser instances");
		}
	}

	public static String getText(WebElement element) {
		try {
			log.info("getting the text of web element");
			return element.getText();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" while getting the text of the web element");
			return null;
		}
	}

	public static void clearTextbox(By by) {
		try {
			log.info("clearing the text in the textbox");
			getElement(by).clear();
		} catch (Exception ex) {
			log.error("Caught an exception: "+ex.getMessage()+" clearing the text in the textbox");
		}
	}

	public static String getAttribute(By by, String value) {
		return getElement(by).getAttribute(value);
	}

	public static void selectByIndex() {

	}

	public static void selectByValue() {

	}

	public static void selectByText() {

	}
	
	public static boolean isEnabled(By by){
		return getElement(by).isEnabled();
	}
	
	public static boolean isSelected(By by){
		return getElement(by).isSelected();
	}
	
	public static boolean isDisplayed(By by){
		return getElement(by).isDisplayed();
	}
	
	public static String getCurrentWindow(){
		return driver.getWindowHandle();
	}
	
	public static Set<String> getAllWindows(){
		return driver.getWindowHandles();
	}
	
	
	
	public static void switchTDefaultPage(String window){
		driver.switchTo().defaultContent();
	}
	
	public static void switchToWindow(String window){
		driver.switchTo().window(window);
	}
	
    public static void closeWindow(String window){
		driver.switchTo().window(window).close();
	}
	
	public static void switchToAlert(){
		driver.switchTo().alert();
	}

	public static void acceptAlert(){
		driver.switchTo().alert().accept();;
	}

	public static void dismissAlert(){
		driver.switchTo().alert().dismiss();
	}
	
	public static void switchToFrame(int index){
		driver.switchTo().frame(index);
	}
	
     public static void switchToFrame(String frameName){
		driver.switchTo().frame(frameName);
	}
	 
     public static void switchToFrame(WebElement element){
		driver.switchTo().frame(element);
	}
	
     public static void JSClick(By by){
    	 WebElement element = getElement(by);
    	 JavascriptExecutor executor = (JavascriptExecutor)driver;
    	 executor.executeScript("arguments[0].click();", element);
     }
     
     public static void JSSendKeys(By by, String text){
    	 
    	 JavascriptExecutor executor = (JavascriptExecutor)driver;
    	 WebElement inputField =getElement(by);
    	 
    	 executor.executeScript("arguments[0].setAttribute('value', '" + text +"')", inputField);

     }
     
     public static void JSSubmit(By by){
    	 
    	 JavascriptExecutor executor = (JavascriptExecutor)driver;
    	 WebElement element = getElement(by);
    	 executor.executeScript("arguments[0].form.submit();", element);

     }
     
     protected static WebElement getElement(By by){
    	 try {
			WebElement myDynamicElement = (new WebDriverWait(driver, 10))
					  .until(ExpectedConditions.presenceOfElementLocated(by));
			 return myDynamicElement;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
	
