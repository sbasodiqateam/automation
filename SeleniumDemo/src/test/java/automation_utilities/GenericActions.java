package automation_utilities;

import java.security.SecureRandom;
import java.util.Random;

public class GenericActions extends BasicLog4jExample{

	static final String stringName = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static final String alphaNumericStringName = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	public static String getRandomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(stringName.charAt(rnd.nextInt(stringName.length())));
		return sb.toString();
	}

	public static int getRandomNumber() {
		Random random  = new Random();
		int value = random.nextInt(100000);
		return value;
	}
	public static int getRandomNumber(int min, int max) {
		Random rand = new Random();
		// nextInt is normally exclusive of the top value,
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}


	public static String getRandomAlphaNumeric(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(alphaNumericStringName.charAt(rnd.nextInt(alphaNumericStringName.length())));
		return sb.toString();
	}
	
	//ToDo getDate

}
