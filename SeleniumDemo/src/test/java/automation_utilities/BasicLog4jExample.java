package automation_utilities;

import java.io.File;
import java.security.SecureRandom;
import java.util.Random;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
 
public class BasicLog4jExample {
	public static String randomString ;
	public static Logger log = null;
	static final String stringName = "abcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	
    static {
    	//Cleanup for previous logs
    	randomString = getRandomString(4);
    	System.setProperty("randomString", "TestReport_"+randomString);
    	 log = Logger.getLogger(BasicLog4jExample.class);
            String log4jConfigFile = System.getProperty("user.dir")
                    + File.separator + "log4j.xml";
            DOMConfigurator.configure(log4jConfigFile);
        
    }

	public static String getRandomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(stringName.charAt(rnd.nextInt(stringName.length())));
		return sb.toString();
	}

}