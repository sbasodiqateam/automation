package automation_utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TestBase extends SeleniumActions {
	
	public static WebDriver driver=null;
	
	public static void setUp(){
		try {
			invokeBrowser();
			openUrl("https://www.freecrm.com/index.html");
			Thread.sleep(2000);
			//enterText(By.name("username"), "gselenium");
			JSSendKeys(By.name("username"), "gselenium");
			Thread.sleep(2000);
			//enterText(By.name("password"), "selenium123");
			JSSendKeys(By.name("password"), "selenium123");
			Thread.sleep(2000);
			//submit(By.name("password"));
			JSSubmit(By.name("password"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void tearDown(){
		try {
			//ToDo logout from App
			driver.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//throw;
		}
	}
}
