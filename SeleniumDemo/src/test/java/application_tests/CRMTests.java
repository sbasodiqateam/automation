package application_tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import automation_utilities.TestBase;

public class CRMTests extends TestBase {
	
	public static WebDriver driver=null;
	protected static By frameSelector=By.xpath("//*[@name='mainpanel']");
	protected static By menuSelector=By.xpath("//*[@id='navMenu']//ul//li");
	protected static String expectedTitle ="CRMPRO";

	
	@BeforeTest
	public void launchApp(){
		try {
			
			TestBase.setUp();
			//logger();
			log.info("Im from setup");
			log.info("Im from setup");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public static void sampleTest() {
		try {
			Thread.sleep(3000);
			String title  = getPageTitle();
			Assert.assertNotNull(title, "Unable to get the title");
			Assert.assertEquals(title,expectedTitle);
			switchToFrame(getElement(frameSelector));
			 List<WebElement> Elements  = getElements(menuSelector);
			// Elements
			 System.out.println(Elements.size());
			 System.out.println("**********************************");
			 for(WebElement element : Elements){
				Assert.assertNotNull(element);
				System.out.println(getText(element));
			 }
			 log.info("Im from Tests");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@AfterTest
	public void closeApp(){
		try {
			closeBrowser();
log.info("Im from tearDown()");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}
	}
