package application_tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class DemoReports {

	public WebDriver driver = null;
	public ExtentReports reports;
	public ExtentTest test;
	

	
	@BeforeMethod
	public void setUp() {
		try {
			reports  = new ExtentReports("./TestReports.html");
			test  = reports.startTest("setUp");
	
			test.log(LogStatus.INFO, "Browser Launched");
			test.log(LogStatus.INFO, "Browser Launched");
			reports.endTest(test);
			reports.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void seleniumTest() {
		try {
			test  = reports.startTest("seleniumTest");
			test.log(LogStatus.INFO, "Test Initiated");
			
			test.log(LogStatus.INFO, "Navigated to the URL");
			if("xyz".equals("abc"))
				test.log(LogStatus.PASS, "Test Pass");
			else
			test.log(LogStatus.FAIL, "Test Fail");
			
			reports.endTest(test);
			reports.flush();
			
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Test Failed: Excepetion "+e);
			reports.endTest(test);
			reports.flush();
		}
	}

	@AfterMethod
	public void afterMethod() {
		try {
			test  = reports.startTest("clenupTests");
			
			test.log(LogStatus.INFO, "Closing the browser and stopping the execution..");
			reports.endTest(test);
			reports.flush();
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}

}
